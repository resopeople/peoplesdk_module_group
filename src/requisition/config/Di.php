<?php

use people_sdk\group\requisition\request\info\factory\model\GroupConfigSndInfoFactory;



return array(
    // Group requisition request sending information services
    // ******************************************************************************

    'people_group_requisition_request_snd_info_factory' => [
        'source' => GroupConfigSndInfoFactory::class,
        'argument' => [
            ['type' => 'dependency', 'value' => 'people_requisition_config'],
            ['type' => 'config', 'value' => 'people/group/requisition/request/snd_info_factory/config']
        ],
        'option' => [
            'shared' => true
        ]
    ]
);
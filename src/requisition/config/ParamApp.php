<?php

use people_sdk\group\requisition\request\info\factory\model\GroupConfigSndInfoFactory;



return array(
    // People configuration
    // ******************************************************************************

    'people' => [
        'requisition' => [
            'request' => [
                'snd_info_factory' => [
                    'config' => [
                        'snd_info_factory' => [
                            ['snd_info_factory' => 'people_group_requisition_request_snd_info_factory']
                        ]
                    ]
                ]
            ]
        ],

        'group' => [
            'requisition' => [
                'request' => [
                    // Group requisition request sending information factory
                    'snd_info_factory' => [
                        /**
                         * Configuration array format:
                         * @see GroupConfigSndInfoFactory configuration format.
                         */
                        'config' => [
                            'group_permission_scope_support_type' => 'header',
                            'group_permission_scope_current_profile_include_config_key' => 'people_group_permission_scope_current_profile_include'
                        ]
                    ]
                ]
            ]
        ]
    ]
);
<?php

use people_sdk\library\requisition\request\info\factory\api\SndInfoFactoryInterface;
use people_sdk\user_profile\user\model\UserProfileEntityFactory;
use people_sdk\group\user_profile\user\model\repository\UserProfileEntityMultiRepository;
use people_sdk\group\user_profile\user\model\repository\UserProfileEntityMultiCollectionRepository;



return array(
    // User profile entity services
    // ******************************************************************************

    'people_group_user_profile_entity_multi_repository' => [
        'source' => UserProfileEntityMultiRepository::class,
        'argument' => [
            ['type' => 'dependency', 'value' => 'people_requisition_persistor'],
            ['type' => 'class', 'value' => SndInfoFactoryInterface::class]
        ],
        'option' => [
            'shared' => true,
        ]
    ],

    'people_group_user_profile_entity_multi_collection_repository' => [
        'source' => UserProfileEntityMultiCollectionRepository::class,
        'argument' => [
            ['type' => 'class', 'value' => UserProfileEntityFactory::class],
            ['type' => 'dependency', 'value' => 'people_group_user_profile_entity_multi_repository']
        ],
        'option' => [
            'shared' => true,
        ]
    ]
);
<?php
/**
 * Description :
 * This class allows to define permission scope module bootstrap class.
 * Permission scope module bootstrap allows to boot permission scope module.
 *
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace people_sdk\module_group\permission_scope\boot;

use liberty_code\framework\bootstrap\model\DefaultBootstrap;

use liberty_code\config\config\api\ConfigInterface;
use liberty_code\framework\application\api\AppInterface;
use liberty_code\framework\framework\config\library\ToolBoxConfig;
use people_sdk\group\role\permission\scope\model\PermScopeEntityFactory;
use people_sdk\module_group\permission_scope\library\ConstPermScope;



class PermScopeBootstrap extends DefaultBootstrap
{
    // ******************************************************************************
    // Properties
    // ******************************************************************************

    /**
     * Init instances table to dissociate this class from parent
     * @var array
     */
    static protected $__instanceTab = array();



    /**
     * DI: Application configuration instance.
     * @var ConfigInterface
     */
    protected $objAppConfig;



    /**
     * DI: Permission scope entity factory instance.
     * @var PermScopeEntityFactory
     */
    protected $objPermScopeEntityFactory;





    // ******************************************************************************
    // Methods
    // ******************************************************************************

    // Constructor / Destructor / Others
    // ******************************************************************************

    /**
     * @inheritdoc
     * @param ConfigInterface $objAppConfig
     * @param PermScopeEntityFactory $objPermScopeEntityFactory
     */
    public function __construct(
        AppInterface $objApp,
        ConfigInterface $objAppConfig,
        PermScopeEntityFactory $objPermScopeEntityFactory
    )
    {
        // Init properties
        $this->objAppConfig = $objAppConfig;
        $this->objPermScopeEntityFactory = $objPermScopeEntityFactory;

        // Call parent constructor
        parent::__construct($objApp, ConstPermScope::MODULE_KEY);
    }





    // Methods execute
    // ******************************************************************************

    /**
     * Boot module.
     */
    public function boot()
    {
        // Set permission scope entity factory configuration, if required
        if(
            is_array($tabConfig = $this->objAppConfig->getValue(
                ToolBoxConfig::getStrPathKeyFromList('people', 'group', 'permission_scope', 'factory', 'config')
            )) &&
            (count($tabConfig) > 0)
        )
        {
            $tabConfig = array_merge($this->objPermScopeEntityFactory->getTabConfig(), $tabConfig);
            $this->objPermScopeEntityFactory->setTabConfig($tabConfig);
        };
    }



}
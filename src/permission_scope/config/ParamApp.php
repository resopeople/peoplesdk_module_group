<?php

use people_sdk\group\role\permission\scope\model\PermScopeEntityFactory;



return array(
    // People configuration
    // ******************************************************************************

    'people' => [
        'group' => [
            'permission_scope' => [
                // Permission scope entity factory
                'factory' => [
                    /**
                     * Configuration array format:
                     * @see PermScopeEntityFactory configuration format.
                     */
                    'config' => [
                        'select_entity_require' => true,
                        'select_entity_create_require' => true,
                        'select_entity_collection_set_require' => true
                    ],

                    /**
                     * User profile entity factory execution configuration array format:
                     * @see PermScopeEntityFactory::setTabUserProfileEntityFactoryExecConfig() configuration format.
                     */
                    'user_profile_factory_execution_config' => [],

                    /**
                     * Application profile entity factory execution configuration array format:
                     * @see PermScopeEntityFactory::setTabAppProfileEntityFactoryExecConfig() configuration format.
                     */
                    'app_profile_factory_execution_config' => [],

                    /**
                     * Group entity factory execution configuration array format:
                     * @see PermScopeEntityFactory::setTabGroupEntityFactoryExecConfig() configuration format.
                     */
                    'group_factory_execution_config' => []
                ]
            ]
        ]
    ]
);
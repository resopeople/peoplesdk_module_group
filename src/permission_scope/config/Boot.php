<?php

use people_sdk\module_group\permission_scope\boot\PermScopeBootstrap;



return array(
    'people_group_permission_scope_bootstrap' => [
        'call' => [
            'class_path_pattern' => PermScopeBootstrap::class . ':boot'
        ]
    ]
);
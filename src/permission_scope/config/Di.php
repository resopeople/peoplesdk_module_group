<?php

use liberty_code\di\provider\api\ProviderInterface;
use people_sdk\library\requisition\request\info\factory\api\SndInfoFactoryInterface;
use people_sdk\user_profile\user\model\UserProfileEntityFactory;
use people_sdk\app_profile\app\model\AppProfileEntityFactory;
use people_sdk\group\group\model\GroupEntityFactory;
use people_sdk\group\role\permission\scope\model\PermScopeEntityCollection;
use people_sdk\group\role\permission\scope\model\PermScopeEntityFactory;
use people_sdk\group\role\permission\scope\model\repository\PermScopeEntityMultiRepository;
use people_sdk\group\role\permission\scope\model\repository\PermScopeEntityMultiCollectionRepository;



return array(
    // Permission scope entity services
    // ******************************************************************************

    'people_group_permission_scope_entity_collection' => [
        'source' => PermScopeEntityCollection::class
    ],

    'people_group_permission_scope_entity_factory_collection' => [
        'source' => PermScopeEntityCollection::class,
        'option' => [
            'shared' => true
        ]
    ],

    'people_group_permission_scope_entity_factory' => [
        'source' => PermScopeEntityFactory::class,
        'argument' => [
            ['type' => 'class', 'value' => ProviderInterface::class],
            ['type' => 'dependency', 'value' => 'people_group_permission_scope_entity_factory_collection'],
            ['type' => 'class', 'value' => UserProfileEntityFactory::class],
            ['type' => 'class', 'value' => AppProfileEntityFactory::class],
            ['type' => 'class', 'value' => GroupEntityFactory::class],
            ['type' => 'config', 'value' => 'people/group/permission_scope/factory/user_profile_factory_execution_config'],
            ['type' => 'config', 'value' => 'people/group/permission_scope/factory/app_profile_factory_execution_config'],
            ['type' => 'config', 'value' => 'people/group/permission_scope/factory/group_factory_execution_config']
        ],
        'option' => [
            'shared' => true
        ]
    ],

    'people_group_permission_scope_entity_multi_repository' => [
        'source' => PermScopeEntityMultiRepository::class,
        'argument' => [
            ['type' => 'dependency', 'value' => 'people_requisition_persistor'],
            ['type' => 'class', 'value' => SndInfoFactoryInterface::class]
        ],
        'option' => [
            'shared' => true,
        ]
    ],

    'people_group_permission_scope_entity_multi_collection_repository' => [
        'source' => PermScopeEntityMultiCollectionRepository::class,
        'argument' => [
            ['type' => 'dependency', 'value' => 'people_group_permission_scope_entity_factory'],
            ['type' => 'dependency', 'value' => 'people_group_permission_scope_entity_multi_repository']
        ],
        'option' => [
            'shared' => true,
        ]
    ],



    // Preferences
    // ******************************************************************************

    PermScopeEntityCollection::class => [
        'set' => ['type' => 'dependency', 'value' => 'people_group_permission_scope_entity_collection']
    ]
);
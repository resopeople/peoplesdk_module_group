<?php

use people_sdk\group\group\model\GroupEntityFactory;



return array(
    // People configuration
    // ******************************************************************************

    'people' => [
        'group' => [
            // Group entity factory
            'factory' => [
                /**
                 * Configuration array format:
                 * @see GroupEntityFactory configuration format.
                 */
                'config' => [
                    'select_entity_require' => true,
                    'select_entity_create_require' => true,
                    'select_entity_collection_set_require' => true
                ]
            ]
        ]
    ]
);
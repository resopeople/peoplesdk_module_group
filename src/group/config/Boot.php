<?php

use people_sdk\module_group\group\boot\GroupBootstrap;



return array(
    'people_group_bootstrap' => [
        'call' => [
            'class_path_pattern' => GroupBootstrap::class . ':boot'
        ]
    ]
);
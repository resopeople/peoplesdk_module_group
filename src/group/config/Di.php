<?php

use liberty_code\di\provider\api\ProviderInterface;
use people_sdk\library\requisition\request\info\factory\api\SndInfoFactoryInterface;
use people_sdk\group\group\model\GroupEntityCollection;
use people_sdk\group\group\model\GroupEntityFactory;
use people_sdk\group\group\model\repository\GroupEntityMultiRepository;
use people_sdk\group\group\model\repository\GroupEntityMultiCollectionRepository;



return array(
    // Group entity services
    // ******************************************************************************

    'people_group_entity_collection' => [
        'source' => GroupEntityCollection::class
    ],

    'people_group_entity_factory_collection' => [
        'source' => GroupEntityCollection::class,
        'option' => [
            'shared' => true
        ]
    ],

    'people_group_entity_factory' => [
        'source' => GroupEntityFactory::class,
        'argument' => [
            ['type' => 'class', 'value' => ProviderInterface::class],
            ['type' => 'dependency', 'value' => 'people_group_entity_factory_collection']
        ],
        'option' => [
            'shared' => true
        ]
    ],

    'people_group_entity_multi_repository' => [
        'source' => GroupEntityMultiRepository::class,
        'argument' => [
            ['type' => 'dependency', 'value' => 'people_requisition_persistor'],
            ['type' => 'class', 'value' => SndInfoFactoryInterface::class]
        ],
        'option' => [
            'shared' => true,
        ]
    ],

    'people_group_entity_multi_collection_repository' => [
        'source' => GroupEntityMultiCollectionRepository::class,
        'argument' => [
            ['type' => 'dependency', 'value' => 'people_group_entity_factory'],
            ['type' => 'dependency', 'value' => 'people_group_entity_multi_repository']
        ],
        'option' => [
            'shared' => true,
        ]
    ],



    // Preferences
    // ******************************************************************************

    GroupEntityCollection::class => [
        'set' => ['type' => 'dependency', 'value' => 'people_group_entity_collection']
    ]
);
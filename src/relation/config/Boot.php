<?php

use people_sdk\module_group\relation\boot\RelationBootstrap;



return array(
    'people_group_relation_bootstrap' => [
        'call' => [
            'class_path_pattern' => RelationBootstrap::class . ':boot'
        ]
    ]
);
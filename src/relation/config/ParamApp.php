<?php

use people_sdk\group\relation\model\RelationEntityFactory;



return array(
    // People configuration
    // ******************************************************************************

    'people' => [
        'group' => [
            'relation' => [
                // Relation entity factory
                'factory' => [
                    /**
                     * Configuration array format:
                     * @see RelationEntityFactory configuration format.
                     */
                    'config' => [
                        'select_entity_require' => true,
                        'select_entity_create_require' => true,
                        'select_entity_collection_set_require' => true
                    ],

                    /**
                     * Member entity factory execution configuration array format:
                     * @see RelationEntityFactory::setTabMemberEntityFactoryExecConfig() configuration format.
                     */
                    'member_factory_execution_config' => []
                ]
            ]
        ]
    ]
);
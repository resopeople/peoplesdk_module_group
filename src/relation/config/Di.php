<?php

use liberty_code\di\provider\api\ProviderInterface;
use people_sdk\library\requisition\request\info\factory\api\SndInfoFactoryInterface;
use people_sdk\group\member\model\MemberEntityFactory;
use people_sdk\group\relation\model\RelationEntityCollection;
use people_sdk\group\relation\model\RelationEntityFactory;
use people_sdk\group\relation\model\repository\RelationEntityMultiRepository;
use people_sdk\group\relation\model\repository\RelationEntityMultiCollectionRepository;



return array(
    // Relation entity services
    // ******************************************************************************

    'people_group_relation_entity_collection' => [
        'source' => RelationEntityCollection::class
    ],

    'people_group_relation_entity_factory_collection' => [
        'source' => RelationEntityCollection::class,
        'option' => [
            'shared' => true
        ]
    ],

    'people_group_relation_entity_factory' => [
        'source' => RelationEntityFactory::class,
        'argument' => [
            ['type' => 'class', 'value' => ProviderInterface::class],
            ['type' => 'dependency', 'value' => 'people_group_relation_entity_factory_collection'],
            ['type' => 'class', 'value' => MemberEntityFactory::class],
            ['type' => 'config', 'value' => 'people/group/relation/factory/member_factory_execution_config']
        ],
        'option' => [
            'shared' => true
        ]
    ],

    'people_group_relation_entity_multi_repository' => [
        'source' => RelationEntityMultiRepository::class,
        'argument' => [
            ['type' => 'dependency', 'value' => 'people_requisition_persistor'],
            ['type' => 'class', 'value' => SndInfoFactoryInterface::class]
        ],
        'option' => [
            'shared' => true,
        ]
    ],

    'people_group_relation_entity_multi_collection_repository' => [
        'source' => RelationEntityMultiCollectionRepository::class,
        'argument' => [
            ['type' => 'dependency', 'value' => 'people_group_relation_entity_factory'],
            ['type' => 'dependency', 'value' => 'people_group_relation_entity_multi_repository']
        ],
        'option' => [
            'shared' => true,
        ]
    ],



    // Preferences
    // ******************************************************************************

    RelationEntityCollection::class => [
        'set' => ['type' => 'dependency', 'value' => 'people_group_relation_entity_collection']
    ]
);
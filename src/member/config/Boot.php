<?php

use people_sdk\module_group\member\boot\MemberBootstrap;



return array(
    'people_group_member_bootstrap' => [
        'call' => [
            'class_path_pattern' => MemberBootstrap::class . ':boot'
        ]
    ]
);
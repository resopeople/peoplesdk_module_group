<?php

// Init var
$strRootPath = dirname(__FILE__) . '/..';

// Include class
include($strRootPath . '/src/group/library/ConstGroup.php');
include($strRootPath . '/src/group/boot/GroupBootstrap.php');

include($strRootPath . '/src/member/library/ConstMember.php');
include($strRootPath . '/src/member/boot/MemberBootstrap.php');

include($strRootPath . '/src/relation/library/ConstRelation.php');
include($strRootPath . '/src/relation/boot/RelationBootstrap.php');

include($strRootPath . '/src/permission_scope/library/ConstPermScope.php');
include($strRootPath . '/src/permission_scope/boot/PermScopeBootstrap.php');

include($strRootPath . '/src/user_profile/library/ConstUserProfile.php');

include($strRootPath . '/src/requisition/library/ConstRequisition.php');